(function ($){

    var scroll_offset = 0;

    $(window).on('scroll', function(e){

        var scroll_top = $(window).scrollTop();

        $('.scrollalong').each(function(){
            var self = $(this);
            var original_offset = self.data('original-offset');
            var self_scroll_offset = self.data('scroll-offset');


//            var pt = self.next().css('padding-top').replace('px', '');

            if(scroll_top >= original_offset.top-self_scroll_offset){
                self.css('position', 'fixed');
                self.show();
                self.css('top', self_scroll_offset);
                self.css('opacity', '0.9');
//                self.next().css('padding-top', pt+self.outerHeight());
            }
            else if(scroll_top <= original_offset.top){
                self.hide();
                self.css('position', 'static');
                self.css('opacity', '1');
                self.css('top', original_offset.top);

//                self.next().css('padding-top', pt-self.outerHeight());
            }
        });

    });

    $.fn.scrollalong = function(){

        return this.each(function(){
            var $this = $(this);
            var clone = $this.clone();

//            $this.addClass('scrollalong');
            clone.addClass('scrollalong');
            clone.css('z-index', 100);

            var original_offset = $this.offset();
//            $this.data('original-offset', original_offset);
//            $this.data('scroll-offset', scroll_offset);
            clone.data('original-offset', original_offset);
            clone.data('scroll-offset', scroll_offset);
            $('body').append(clone.hide());

//            $this.data('clone', clone);

            scroll_offset += $this.outerHeight();

        });
    };


})( jQuery );
