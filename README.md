jquery.scrollalong
=========================

a plugin to make elements scroll with window

ToDo
=========================
* Handle multiple 'scroll-along's


Example
=========================


> index.html
```html
<style>div{width: 100%; padding: 1em; background-color: gray;}</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="jquery.scrollalong.js"></script>
<div>Will stay</div>
<div class="scroll-along">Will Scroll with window</div>
<div>
  ...scroll...<br><br><br><br><br><br><br><br><br><br><br><br>
  ...scroll...<br><br><br><br><br><br><br><br><br><br><br><br>
  ...scroll...<br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<script>
$('.scroll-along').scrollalong();
</script>
```
